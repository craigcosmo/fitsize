
//call
//$('.wantedToFit').fitSize({
// 	target:$(".role_model"),
// 	width:true,
// 	height:true
// })

(function($){
	$.fn.fitSize = function(options){
		let defaults = {
			target:'',
			width: true,
			height: true
		},
		settings = $.extend({},defaults, options)
		
		return this.each(function(){
			
			let $this=$(this)
			if(settings.width == true) {
				let fakeWidth = $this.outerWidth(true)-$this.width()
				$this.width(settings.target.width()-fakeWidth)
			}
			if(settings.height == true){
				let fakeHeight = $this.outerHeight(true)-$this.height()
				$this.height(settings.target.height()-fakeHeight)
			}
		})
	}
})(jQuery)